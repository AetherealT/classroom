﻿
namespace classroom
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.менюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.калькуляторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.игрыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.паукToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пасьянсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.косынкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.подключениеКБазеДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверкаПодключенияКБазеДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.панельАдминистратораToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView_timetable = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView_journal = new System.Windows.Forms.DataGridView();
            this.tabControl_timetable = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.button34 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.button15 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.dataGridView_ved = new System.Windows.Forms.DataGridView();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.button35 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.monthCalendar6 = new System.Windows.Forms.MonthCalendar();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridView_plan = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView_final = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.button39 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridView_meeting = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.button41 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.monthCalendar3 = new System.Windows.Forms.MonthCalendar();
            this.monthCalendar2 = new System.Windows.Forms.MonthCalendar();
            this.label26 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView_lgot = new System.Windows.Forms.DataGridView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.button44 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.monthCalendar4 = new System.Windows.Forms.MonthCalendar();
            this.monthCalendar5 = new System.Windows.Forms.MonthCalendar();
            this.label27 = new System.Windows.Forms.Label();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.button8 = new System.Windows.Forms.Button();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dataGridView_uchet = new System.Windows.Forms.DataGridView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.monthCalendar7 = new System.Windows.Forms.MonthCalendar();
            this.label32 = new System.Windows.Forms.Label();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGridView_students = new System.Windows.Forms.DataGridView();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.button47 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dataGridView_parrents = new System.Windows.Forms.DataGridView();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.dataGridView_teach = new System.Windows.Forms.DataGridView();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_timetable)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_journal)).BeginInit();
            this.tabControl_timetable.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ved)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_plan)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_final)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_meeting)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_lgot)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_uchet)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_students)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_parrents)).BeginInit();
            this.tabPage9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_teach)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-2, -3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1480, 35);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(37, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(572, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Система автоматизации рабочего процесса для классного руководителя";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::classroom.Properties.Resources._2_3pRJ22SqSeYLFGOZvO6C2w;
            this.pictureBox1.Location = new System.Drawing.Point(7, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(1414, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "-";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1445, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.менюToolStripMenuItem,
            this.настройкиToolStripMenuItem,
            this.панельАдминистратораToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(-6, 32);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(294, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // менюToolStripMenuItem
            // 
            this.менюToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкаToolStripMenuItem,
            this.калькуляторToolStripMenuItem,
            this.игрыToolStripMenuItem});
            this.менюToolStripMenuItem.Name = "менюToolStripMenuItem";
            this.менюToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.менюToolStripMenuItem.Text = "Меню";
            // 
            // настройкаToolStripMenuItem
            // 
            this.настройкаToolStripMenuItem.Name = "настройкаToolStripMenuItem";
            this.настройкаToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.настройкаToolStripMenuItem.Text = "Блокнот";
            // 
            // калькуляторToolStripMenuItem
            // 
            this.калькуляторToolStripMenuItem.Name = "калькуляторToolStripMenuItem";
            this.калькуляторToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.калькуляторToolStripMenuItem.Text = "Калькулятор";
            // 
            // игрыToolStripMenuItem
            // 
            this.игрыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.паукToolStripMenuItem,
            this.пасьянсToolStripMenuItem,
            this.косынкаToolStripMenuItem});
            this.игрыToolStripMenuItem.Name = "игрыToolStripMenuItem";
            this.игрыToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.игрыToolStripMenuItem.Text = "Игры";
            // 
            // паукToolStripMenuItem
            // 
            this.паукToolStripMenuItem.Name = "паукToolStripMenuItem";
            this.паукToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.паукToolStripMenuItem.Text = "Паук";
            // 
            // пасьянсToolStripMenuItem
            // 
            this.пасьянсToolStripMenuItem.Name = "пасьянсToolStripMenuItem";
            this.пасьянсToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.пасьянсToolStripMenuItem.Text = "Пасьянс";
            // 
            // косынкаToolStripMenuItem
            // 
            this.косынкаToolStripMenuItem.Name = "косынкаToolStripMenuItem";
            this.косынкаToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.косынкаToolStripMenuItem.Text = "Косынка";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.подключениеКБазеДанныхToolStripMenuItem,
            this.проверкаПодключенияКБазеДанныхToolStripMenuItem});
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // подключениеКБазеДанныхToolStripMenuItem
            // 
            this.подключениеКБазеДанныхToolStripMenuItem.Name = "подключениеКБазеДанныхToolStripMenuItem";
            this.подключениеКБазеДанныхToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.подключениеКБазеДанныхToolStripMenuItem.Text = "Подключение к базе данных";
            // 
            // проверкаПодключенияКБазеДанныхToolStripMenuItem
            // 
            this.проверкаПодключенияКБазеДанныхToolStripMenuItem.Name = "проверкаПодключенияКБазеДанныхToolStripMenuItem";
            this.проверкаПодключенияКБазеДанныхToolStripMenuItem.Size = new System.Drawing.Size(287, 22);
            this.проверкаПодключенияКБазеДанныхToolStripMenuItem.Text = "Проверка подключения к базе данных";
            // 
            // панельАдминистратораToolStripMenuItem
            // 
            this.панельАдминистратораToolStripMenuItem.Name = "панельАдминистратораToolStripMenuItem";
            this.панельАдминистратораToolStripMenuItem.Size = new System.Drawing.Size(154, 20);
            this.панельАдминистратораToolStripMenuItem.Text = "Панель администратора";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button48);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1455, 661);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Расписание";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(1045, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(405, 57);
            this.button2.TabIndex = 4;
            this.button2.Text = "Обновить данные";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView_timetable);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1035, 649);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Данные";
            // 
            // dataGridView_timetable
            // 
            this.dataGridView_timetable.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_timetable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_timetable.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_timetable.Name = "dataGridView_timetable";
            this.dataGridView_timetable.Size = new System.Drawing.Size(1023, 624);
            this.dataGridView_timetable.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1455, 661);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Журнал";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button52);
            this.groupBox11.Controls.Add(this.button32);
            this.groupBox11.Controls.Add(this.button31);
            this.groupBox11.Controls.Add(this.button30);
            this.groupBox11.Controls.Add(this.button12);
            this.groupBox11.Controls.Add(this.button14);
            this.groupBox11.Controls.Add(this.button3);
            this.groupBox11.Controls.Add(this.textBox3);
            this.groupBox11.Controls.Add(this.textBox2);
            this.groupBox11.Controls.Add(this.textBox1);
            this.groupBox11.Controls.Add(this.label7);
            this.groupBox11.Controls.Add(this.label6);
            this.groupBox11.Controls.Add(this.listBox1);
            this.groupBox11.Controls.Add(this.label5);
            this.groupBox11.Controls.Add(this.label4);
            this.groupBox11.Location = new System.Drawing.Point(1080, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(364, 640);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Добавление данных";
            // 
            // button32
            // 
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button32.Location = new System.Drawing.Point(188, 543);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(166, 40);
            this.button32.TabIndex = 12;
            this.button32.Text = "Отчет по предмету";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button31.Location = new System.Drawing.Point(188, 497);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(166, 40);
            this.button31.TabIndex = 11;
            this.button31.Text = "Отчет по ученику";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button30.Location = new System.Drawing.Point(188, 451);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(166, 40);
            this.button30.TabIndex = 10;
            this.button30.Text = "Отчет по оценкам";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button12.Location = new System.Drawing.Point(15, 545);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(166, 38);
            this.button12.TabIndex = 9;
            this.button12.Text = "Обновить данные";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button14.Location = new System.Drawing.Point(15, 497);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(166, 42);
            this.button14.TabIndex = 8;
            this.button14.Text = "Удалить данные";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Location = new System.Drawing.Point(15, 451);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(166, 40);
            this.button3.TabIndex = 8;
            this.button3.Text = "Добавить оценку";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox3.Location = new System.Drawing.Point(223, 30);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(131, 31);
            this.textBox3.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox2.Location = new System.Drawing.Point(219, 414);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(135, 31);
            this.textBox2.TabIndex = 6;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox1.Location = new System.Drawing.Point(223, 67);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 31);
            this.textBox1.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(14, 417);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 25);
            this.label7.TabIndex = 4;
            this.label7.Text = "Оценка : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(0, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 25);
            this.label6.TabIndex = 3;
            this.label6.Text = "Предмет : ";
            // 
            // listBox1
            // 
            this.listBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 25;
            this.listBox1.Items.AddRange(new object[] {
            "Математика",
            "Русский язык",
            "Английский язык",
            "История России",
            "Всеобщая история",
            "Обществознание",
            "Окружающий мир",
            "Немецкий язык",
            "Французкий язык",
            "Физика",
            "Алгебра",
            "Геометрия",
            "История родного края",
            "Информатика",
            "Черчение",
            "Изобразительное исскуство",
            "Здоровый образ жизни",
            "Безопасность жизнедеятельности",
            "Астраномия",
            "Литература",
            "Физкультура",
            "Технология (мальчики)",
            "Технология (девочки)",
            "География"});
            this.listBox1.Location = new System.Drawing.Point(124, 104);
            this.listBox1.Name = "listBox1";
            this.listBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.listBox1.Size = new System.Drawing.Size(230, 304);
            this.listBox1.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(1, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(209, 25);
            this.label5.TabIndex = 1;
            this.label5.Text = "Фамилия ученика : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(1, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Имя ученика : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView_journal);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1068, 649);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Данные";
            // 
            // dataGridView_journal
            // 
            this.dataGridView_journal.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_journal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_journal.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_journal.Name = "dataGridView_journal";
            this.dataGridView_journal.Size = new System.Drawing.Size(1056, 624);
            this.dataGridView_journal.TabIndex = 0;
            // 
            // tabControl_timetable
            // 
            this.tabControl_timetable.Controls.Add(this.tabPage1);
            this.tabControl_timetable.Controls.Add(this.tabPage11);
            this.tabControl_timetable.Controls.Add(this.tabPage2);
            this.tabControl_timetable.Controls.Add(this.tabPage10);
            this.tabControl_timetable.Controls.Add(this.tabPage3);
            this.tabControl_timetable.Controls.Add(this.tabPage4);
            this.tabControl_timetable.Controls.Add(this.tabPage5);
            this.tabControl_timetable.Controls.Add(this.tabPage6);
            this.tabControl_timetable.Controls.Add(this.tabPage7);
            this.tabControl_timetable.Controls.Add(this.tabPage8);
            this.tabControl_timetable.Controls.Add(this.tabPage9);
            this.tabControl_timetable.Location = new System.Drawing.Point(5, 60);
            this.tabControl_timetable.Name = "tabControl_timetable";
            this.tabControl_timetable.SelectedIndex = 0;
            this.tabControl_timetable.Size = new System.Drawing.Size(1463, 687);
            this.tabControl_timetable.TabIndex = 2;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.groupBox20);
            this.tabPage11.Controls.Add(this.groupBox19);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(1455, 661);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "Ведомость";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.button53);
            this.groupBox20.Controls.Add(this.button34);
            this.groupBox20.Controls.Add(this.button33);
            this.groupBox20.Controls.Add(this.label45);
            this.groupBox20.Controls.Add(this.textBox37);
            this.groupBox20.Controls.Add(this.button15);
            this.groupBox20.Controls.Add(this.button13);
            this.groupBox20.Controls.Add(this.label40);
            this.groupBox20.Controls.Add(this.textBox36);
            this.groupBox20.Controls.Add(this.textBox35);
            this.groupBox20.Controls.Add(this.label41);
            this.groupBox20.Controls.Add(this.button11);
            this.groupBox20.Controls.Add(this.textBox32);
            this.groupBox20.Controls.Add(this.textBox33);
            this.groupBox20.Controls.Add(this.textBox34);
            this.groupBox20.Controls.Add(this.label42);
            this.groupBox20.Controls.Add(this.label51);
            this.groupBox20.Controls.Add(this.label52);
            this.groupBox20.Location = new System.Drawing.Point(1086, 3);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(364, 649);
            this.groupBox20.TabIndex = 3;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Работа с данными";
            // 
            // button34
            // 
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button34.Location = new System.Drawing.Point(6, 530);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(341, 41);
            this.button34.TabIndex = 19;
            this.button34.Text = "Общее количество пропусков";
            this.button34.UseVisualStyleBackColor = true;
            // 
            // button33
            // 
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button33.Location = new System.Drawing.Point(6, 483);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(341, 41);
            this.button33.TabIndex = 18;
            this.button33.Text = "Общее количество прогулов";
            this.button33.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.Location = new System.Drawing.Point(1, 287);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(231, 25);
            this.label45.TabIndex = 17;
            this.label45.Text = "Причина не по уваж.: ";
            // 
            // textBox37
            // 
            this.textBox37.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox37.Location = new System.Drawing.Point(6, 315);
            this.textBox37.Multiline = true;
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(341, 68);
            this.textBox37.TabIndex = 16;
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button15.Location = new System.Drawing.Point(8, 436);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(339, 41);
            this.button15.TabIndex = 15;
            this.button15.Text = "Обновить данные";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button13.Location = new System.Drawing.Point(187, 389);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(160, 41);
            this.button13.TabIndex = 14;
            this.button13.Text = "Удалить запись";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.Location = new System.Drawing.Point(1, 184);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(207, 25);
            this.label40.TabIndex = 13;
            this.label40.Text = "Причина по уваж. : ";
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox36.Location = new System.Drawing.Point(6, 212);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(341, 66);
            this.textBox36.TabIndex = 12;
            // 
            // textBox35
            // 
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox35.Location = new System.Drawing.Point(216, 141);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(131, 31);
            this.textBox35.TabIndex = 11;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(1, 147);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(205, 25);
            this.label41.TabIndex = 9;
            this.label41.Text = "Не уважительной : ";
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button11.Location = new System.Drawing.Point(6, 389);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(175, 41);
            this.button11.TabIndex = 8;
            this.button11.Text = "Добавить запись";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // textBox32
            // 
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox32.Location = new System.Drawing.Point(216, 30);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(131, 31);
            this.textBox32.TabIndex = 7;
            // 
            // textBox33
            // 
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox33.Location = new System.Drawing.Point(216, 104);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(131, 31);
            this.textBox33.TabIndex = 6;
            // 
            // textBox34
            // 
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox34.Location = new System.Drawing.Point(216, 67);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(131, 31);
            this.textBox34.TabIndex = 5;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(1, 110);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(205, 25);
            this.label42.TabIndex = 4;
            this.label42.Text = "По уважительной : ";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label51.Location = new System.Drawing.Point(1, 33);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(209, 25);
            this.label51.TabIndex = 1;
            this.label51.Text = "Фамилия ученика : ";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label52.Location = new System.Drawing.Point(1, 70);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(158, 25);
            this.label52.TabIndex = 0;
            this.label52.Text = "Имя ученика : ";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.dataGridView_ved);
            this.groupBox19.Location = new System.Drawing.Point(3, 3);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(1068, 649);
            this.groupBox19.TabIndex = 2;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Данные";
            // 
            // dataGridView_ved
            // 
            this.dataGridView_ved.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_ved.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ved.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_ved.Name = "dataGridView_ved";
            this.dataGridView_ved.Size = new System.Drawing.Size(1056, 624);
            this.dataGridView_ved.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.groupBox13);
            this.tabPage10.Controls.Add(this.groupBox4);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(1455, 661);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "План мероприятий";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.button35);
            this.groupBox13.Controls.Add(this.button17);
            this.groupBox13.Controls.Add(this.button16);
            this.groupBox13.Controls.Add(this.monthCalendar6);
            this.groupBox13.Controls.Add(this.label18);
            this.groupBox13.Controls.Add(this.textBox10);
            this.groupBox13.Controls.Add(this.textBox11);
            this.groupBox13.Controls.Add(this.label14);
            this.groupBox13.Controls.Add(this.textBox12);
            this.groupBox13.Controls.Add(this.label15);
            this.groupBox13.Controls.Add(this.button5);
            this.groupBox13.Controls.Add(this.textBox14);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.label17);
            this.groupBox13.Location = new System.Drawing.Point(1086, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(364, 640);
            this.groupBox13.TabIndex = 4;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Работа с данными";
            // 
            // button35
            // 
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button35.Location = new System.Drawing.Point(6, 594);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(352, 38);
            this.button35.TabIndex = 18;
            this.button35.Text = "Эспорт данных";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button17.Location = new System.Drawing.Point(6, 550);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(352, 38);
            this.button17.TabIndex = 17;
            this.button17.Text = "Обновить данные";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button16.Location = new System.Drawing.Point(5, 506);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(352, 38);
            this.button16.TabIndex = 16;
            this.button16.Text = "Удалить мероприятие";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // monthCalendar6
            // 
            this.monthCalendar6.Location = new System.Drawing.Point(192, 16);
            this.monthCalendar6.Name = "monthCalendar6";
            this.monthCalendar6.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(0, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 25);
            this.label18.TabIndex = 14;
            this.label18.Text = "Дата : ";
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox10.Location = new System.Drawing.Point(4, 238);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(352, 52);
            this.textBox10.TabIndex = 13;
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox11.Location = new System.Drawing.Point(214, 388);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(142, 31);
            this.textBox11.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(-1, 391);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(200, 25);
            this.label14.TabIndex = 11;
            this.label14.Text = "Кол-во присуств. : ";
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox12.Location = new System.Drawing.Point(214, 425);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(142, 31);
            this.textBox12.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(-1, 428);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(186, 25);
            this.label15.TabIndex = 9;
            this.label15.Text = "Кол-во отсуств. : ";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5.Location = new System.Drawing.Point(5, 462);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(352, 38);
            this.button5.TabIndex = 8;
            this.button5.Text = "Добавить мероприятие";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox14.Location = new System.Drawing.Point(4, 327);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(352, 49);
            this.textBox14.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(5, 299);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 25);
            this.label16.TabIndex = 4;
            this.label16.Text = "Название : ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(-1, 210);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 25);
            this.label17.TabIndex = 3;
            this.label17.Text = "Место : ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dataGridView_plan);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1077, 640);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Данные";
            // 
            // dataGridView_plan
            // 
            this.dataGridView_plan.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_plan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_plan.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_plan.Name = "dataGridView_plan";
            this.dataGridView_plan.Size = new System.Drawing.Size(1065, 615);
            this.dataGridView_plan.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox12);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1455, 661);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Итоговая аттестация";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.button54);
            this.groupBox12.Controls.Add(this.button36);
            this.groupBox12.Controls.Add(this.button37);
            this.groupBox12.Controls.Add(this.button38);
            this.groupBox12.Controls.Add(this.button18);
            this.groupBox12.Controls.Add(this.button19);
            this.groupBox12.Controls.Add(this.listBox2);
            this.groupBox12.Controls.Add(this.button4);
            this.groupBox12.Controls.Add(this.textBox4);
            this.groupBox12.Controls.Add(this.textBox5);
            this.groupBox12.Controls.Add(this.textBox6);
            this.groupBox12.Controls.Add(this.label8);
            this.groupBox12.Controls.Add(this.label9);
            this.groupBox12.Controls.Add(this.label10);
            this.groupBox12.Controls.Add(this.label11);
            this.groupBox12.Location = new System.Drawing.Point(1086, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(364, 655);
            this.groupBox12.TabIndex = 3;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Работа с данными";
            // 
            // button36
            // 
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button36.Location = new System.Drawing.Point(192, 546);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(166, 40);
            this.button36.TabIndex = 15;
            this.button36.Text = "Отчет по предмету";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button37.Location = new System.Drawing.Point(192, 500);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(166, 40);
            this.button37.TabIndex = 14;
            this.button37.Text = "Отчет по ученику";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // button38
            // 
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button38.Location = new System.Drawing.Point(192, 454);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(166, 40);
            this.button38.TabIndex = 13;
            this.button38.Text = "Отчет по оценкам";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button18.Location = new System.Drawing.Point(5, 548);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(179, 38);
            this.button18.TabIndex = 11;
            this.button18.Text = "Обновить данные";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button19.Location = new System.Drawing.Point(5, 500);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(179, 40);
            this.button19.TabIndex = 10;
            this.button19.Text = "Удалить данные";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // listBox2
            // 
            this.listBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 25;
            this.listBox2.Items.AddRange(new object[] {
            "Математика",
            "Русский язык",
            "Английский язык",
            "История России",
            "Всеобщая история",
            "Обществознание",
            "Окружающий мир",
            "Немецкий язык",
            "Французкий язык",
            "Физика",
            "Алгебра",
            "Геометрия",
            "История родного края",
            "Информатика",
            "Черчение",
            "Изобразительное исскуство",
            "Здоровый образ жизни",
            "Безопасность жизнедеятельности",
            "Астраномия",
            "Литература",
            "Физкультура",
            "Технология (мальчики)",
            "Технология (девочки)",
            "География"});
            this.listBox2.Location = new System.Drawing.Point(130, 107);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(234, 304);
            this.listBox2.TabIndex = 9;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Location = new System.Drawing.Point(6, 455);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(178, 39);
            this.button4.TabIndex = 8;
            this.button4.Text = "Добавить оценку";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox4.Location = new System.Drawing.Point(216, 30);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(142, 31);
            this.textBox4.TabIndex = 7;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox5.Location = new System.Drawing.Point(130, 417);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(234, 31);
            this.textBox5.TabIndex = 6;
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox6.Location = new System.Drawing.Point(216, 67);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(142, 31);
            this.textBox6.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(6, 420);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 25);
            this.label8.TabIndex = 4;
            this.label8.Text = "Оценка :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(0, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 25);
            this.label9.TabIndex = 3;
            this.label9.Text = "Предмет : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(1, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(209, 25);
            this.label10.TabIndex = 1;
            this.label10.Text = "Фамилия ученика : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(1, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(158, 25);
            this.label11.TabIndex = 0;
            this.label11.Text = "Имя ученика : ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView_final);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1074, 655);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Данные";
            // 
            // dataGridView_final
            // 
            this.dataGridView_final.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_final.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_final.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_final.Name = "dataGridView_final";
            this.dataGridView_final.Size = new System.Drawing.Size(1062, 630);
            this.dataGridView_final.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox14);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1455, 661);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Родительское собрание";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.button39);
            this.groupBox14.Controls.Add(this.button21);
            this.groupBox14.Controls.Add(this.button20);
            this.groupBox14.Controls.Add(this.monthCalendar1);
            this.groupBox14.Controls.Add(this.textBox8);
            this.groupBox14.Controls.Add(this.label12);
            this.groupBox14.Controls.Add(this.button6);
            this.groupBox14.Controls.Add(this.label21);
            this.groupBox14.Location = new System.Drawing.Point(1053, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(397, 429);
            this.groupBox14.TabIndex = 5;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Работа с данными";
            // 
            // button39
            // 
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button39.Location = new System.Drawing.Point(9, 382);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(382, 40);
            this.button39.TabIndex = 16;
            this.button39.Text = "Экспорт данных";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button21.Location = new System.Drawing.Point(9, 336);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(382, 40);
            this.button21.TabIndex = 15;
            this.button21.Text = "Обновить";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button20.Location = new System.Drawing.Point(9, 289);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(382, 41);
            this.button20.TabIndex = 14;
            this.button20.Text = "Удалить запись";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(229, 16);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 13;
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox8.Location = new System.Drawing.Point(229, 190);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(162, 31);
            this.textBox8.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(4, 196);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(165, 25);
            this.label12.TabIndex = 11;
            this.label12.Text = "Повестка дня : ";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button6.Location = new System.Drawing.Point(9, 239);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(382, 44);
            this.button6.TabIndex = 8;
            this.button6.Text = "Добавить запись";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(4, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 25);
            this.label21.TabIndex = 3;
            this.label21.Text = "Дата : ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dataGridView_meeting);
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1044, 655);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Данные";
            // 
            // dataGridView_meeting
            // 
            this.dataGridView_meeting.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_meeting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_meeting.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_meeting.Name = "dataGridView_meeting";
            this.dataGridView_meeting.Size = new System.Drawing.Size(1032, 630);
            this.dataGridView_meeting.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox15);
            this.tabPage5.Controls.Add(this.groupBox6);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1455, 661);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Льготы ";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.button49);
            this.groupBox15.Controls.Add(this.button41);
            this.groupBox15.Controls.Add(this.button40);
            this.groupBox15.Controls.Add(this.button23);
            this.groupBox15.Controls.Add(this.button22);
            this.groupBox15.Controls.Add(this.monthCalendar3);
            this.groupBox15.Controls.Add(this.monthCalendar2);
            this.groupBox15.Controls.Add(this.label26);
            this.groupBox15.Controls.Add(this.listBox3);
            this.groupBox15.Controls.Add(this.button7);
            this.groupBox15.Controls.Add(this.textBox7);
            this.groupBox15.Controls.Add(this.textBox17);
            this.groupBox15.Controls.Add(this.label13);
            this.groupBox15.Controls.Add(this.label20);
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this.label25);
            this.groupBox15.Location = new System.Drawing.Point(1022, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(428, 655);
            this.groupBox15.TabIndex = 4;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Работа с данными";
            // 
            // button41
            // 
            this.button41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button41.Location = new System.Drawing.Point(6, 383);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(166, 40);
            this.button41.TabIndex = 16;
            this.button41.Text = "Отчет по льготам";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // button40
            // 
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button40.Location = new System.Drawing.Point(6, 337);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(166, 40);
            this.button40.TabIndex = 15;
            this.button40.Text = "Отчет по ученику";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button23.Location = new System.Drawing.Point(6, 599);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(158, 39);
            this.button23.TabIndex = 14;
            this.button23.Text = "Обновить данные";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button22.Location = new System.Drawing.Point(6, 554);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(158, 39);
            this.button22.TabIndex = 13;
            this.button22.Text = "Удалить запись";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // monthCalendar3
            // 
            this.monthCalendar3.Location = new System.Drawing.Point(247, 487);
            this.monthCalendar3.Name = "monthCalendar3";
            this.monthCalendar3.TabIndex = 12;
            // 
            // monthCalendar2
            // 
            this.monthCalendar2.Location = new System.Drawing.Point(247, 309);
            this.monthCalendar2.Name = "monthCalendar2";
            this.monthCalendar2.TabIndex = 11;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(1, 481);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(197, 25);
            this.label26.TabIndex = 10;
            this.label26.Text = "Дата окончания  : ";
            // 
            // listBox3
            // 
            this.listBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 25;
            this.listBox3.Items.AddRange(new object[] {
            "На бесплатное питание",
            "На бесплатный проезд",
            "Пенсия по потери кормильца"});
            this.listBox3.Location = new System.Drawing.Point(110, 97);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(301, 204);
            this.listBox3.TabIndex = 9;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button7.Location = new System.Drawing.Point(6, 509);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(158, 39);
            this.button7.TabIndex = 8;
            this.button7.Text = "Добавить запись";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox7.Location = new System.Drawing.Point(221, 22);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(190, 31);
            this.textBox7.TabIndex = 7;
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox17.Location = new System.Drawing.Point(221, 59);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(190, 31);
            this.textBox17.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(1, 309);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 25);
            this.label13.TabIndex = 4;
            this.label13.Text = "Дата начала : ";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(6, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 25);
            this.label20.TabIndex = 3;
            this.label20.Text = "Льгота : ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(6, 25);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(209, 25);
            this.label24.TabIndex = 1;
            this.label24.Text = "Фамилия ученика : ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(6, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(158, 25);
            this.label25.TabIndex = 0;
            this.label25.Text = "Имя ученика : ";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGridView_lgot);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1010, 655);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Данные";
            // 
            // dataGridView_lgot
            // 
            this.dataGridView_lgot.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_lgot.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_lgot.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_lgot.Name = "dataGridView_lgot";
            this.dataGridView_lgot.Size = new System.Drawing.Size(998, 630);
            this.dataGridView_lgot.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox16);
            this.tabPage6.Controls.Add(this.groupBox7);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1455, 661);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Учет учеников";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.button50);
            this.groupBox16.Controls.Add(this.button44);
            this.groupBox16.Controls.Add(this.button43);
            this.groupBox16.Controls.Add(this.button42);
            this.groupBox16.Controls.Add(this.button25);
            this.groupBox16.Controls.Add(this.button24);
            this.groupBox16.Controls.Add(this.monthCalendar4);
            this.groupBox16.Controls.Add(this.monthCalendar5);
            this.groupBox16.Controls.Add(this.label27);
            this.groupBox16.Controls.Add(this.listBox4);
            this.groupBox16.Controls.Add(this.button8);
            this.groupBox16.Controls.Add(this.textBox9);
            this.groupBox16.Controls.Add(this.textBox19);
            this.groupBox16.Controls.Add(this.label28);
            this.groupBox16.Controls.Add(this.label29);
            this.groupBox16.Controls.Add(this.label30);
            this.groupBox16.Controls.Add(this.label31);
            this.groupBox16.Location = new System.Drawing.Point(1022, 3);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(428, 649);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Работа с данными";
            // 
            // button44
            // 
            this.button44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button44.Location = new System.Drawing.Point(247, 534);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(164, 39);
            this.button44.TabIndex = 17;
            this.button44.Text = "Поиск ученика";
            this.button44.UseVisualStyleBackColor = true;
            // 
            // button43
            // 
            this.button43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button43.Location = new System.Drawing.Point(247, 579);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(164, 39);
            this.button43.TabIndex = 16;
            this.button43.Text = "Подсчет учеников";
            this.button43.UseVisualStyleBackColor = true;
            // 
            // button42
            // 
            this.button42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button42.Location = new System.Drawing.Point(6, 579);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(229, 39);
            this.button42.TabIndex = 15;
            this.button42.Text = "Отчет";
            this.button42.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button25.Location = new System.Drawing.Point(6, 534);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(229, 39);
            this.button25.TabIndex = 14;
            this.button25.Text = "Обновить данные";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button24.Location = new System.Drawing.Point(6, 444);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(229, 39);
            this.button24.TabIndex = 13;
            this.button24.Text = "Добавить запись";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // monthCalendar4
            // 
            this.monthCalendar4.Location = new System.Drawing.Point(247, 360);
            this.monthCalendar4.Name = "monthCalendar4";
            this.monthCalendar4.TabIndex = 12;
            // 
            // monthCalendar5
            // 
            this.monthCalendar5.Location = new System.Drawing.Point(247, 188);
            this.monthCalendar5.Name = "monthCalendar5";
            this.monthCalendar5.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(1, 360);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(197, 25);
            this.label27.TabIndex = 10;
            this.label27.Text = "Дата окончания  : ";
            // 
            // listBox4
            // 
            this.listBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.listBox4.FormattingEnabled = true;
            this.listBox4.ItemHeight = 25;
            this.listBox4.Items.AddRange(new object[] {
            "ПДН",
            "Неблагополучная семья",
            "Инвалид"});
            this.listBox4.Location = new System.Drawing.Point(160, 97);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(251, 79);
            this.listBox4.TabIndex = 9;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button8.Location = new System.Drawing.Point(6, 489);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(229, 39);
            this.button8.TabIndex = 8;
            this.button8.Text = "Удалить запись";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox9.Location = new System.Drawing.Point(252, 22);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(159, 31);
            this.textBox9.TabIndex = 7;
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox19.Location = new System.Drawing.Point(252, 59);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(159, 31);
            this.textBox19.TabIndex = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(1, 188);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(157, 25);
            this.label28.TabIndex = 4;
            this.label28.Text = "Дата начала : ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(6, 101);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 25);
            this.label29.TabIndex = 3;
            this.label29.Text = "Учет : ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(6, 25);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(209, 25);
            this.label30.TabIndex = 1;
            this.label30.Text = "Фамилия ученика : ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(6, 65);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(158, 25);
            this.label31.TabIndex = 0;
            this.label31.Text = "Имя ученика : ";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dataGridView_uchet);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1013, 655);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Данные";
            // 
            // dataGridView_uchet
            // 
            this.dataGridView_uchet.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_uchet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_uchet.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_uchet.Name = "dataGridView_uchet";
            this.dataGridView_uchet.Size = new System.Drawing.Size(1001, 630);
            this.dataGridView_uchet.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox17);
            this.tabPage7.Controls.Add(this.groupBox8);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1455, 661);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Данные учеников";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.button51);
            this.groupBox17.Controls.Add(this.button46);
            this.groupBox17.Controls.Add(this.button45);
            this.groupBox17.Controls.Add(this.textBox13);
            this.groupBox17.Controls.Add(this.textBox15);
            this.groupBox17.Controls.Add(this.label19);
            this.groupBox17.Controls.Add(this.label22);
            this.groupBox17.Controls.Add(this.button27);
            this.groupBox17.Controls.Add(this.button26);
            this.groupBox17.Controls.Add(this.textBox24);
            this.groupBox17.Controls.Add(this.textBox23);
            this.groupBox17.Controls.Add(this.listBox6);
            this.groupBox17.Controls.Add(this.label39);
            this.groupBox17.Controls.Add(this.textBox22);
            this.groupBox17.Controls.Add(this.label38);
            this.groupBox17.Controls.Add(this.label37);
            this.groupBox17.Controls.Add(this.monthCalendar7);
            this.groupBox17.Controls.Add(this.label32);
            this.groupBox17.Controls.Add(this.listBox5);
            this.groupBox17.Controls.Add(this.button9);
            this.groupBox17.Controls.Add(this.textBox20);
            this.groupBox17.Controls.Add(this.textBox21);
            this.groupBox17.Controls.Add(this.label33);
            this.groupBox17.Controls.Add(this.label34);
            this.groupBox17.Controls.Add(this.label35);
            this.groupBox17.Controls.Add(this.label36);
            this.groupBox17.Location = new System.Drawing.Point(1012, 12);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(438, 640);
            this.groupBox17.TabIndex = 6;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Работа с данными";
            // 
            // button46
            // 
            this.button46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button46.Location = new System.Drawing.Point(231, 518);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(197, 39);
            this.button46.TabIndex = 26;
            this.button46.Text = "Сортировка по группе";
            this.button46.UseVisualStyleBackColor = true;
            // 
            // button45
            // 
            this.button45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button45.Location = new System.Drawing.Point(231, 472);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(197, 39);
            this.button45.TabIndex = 25;
            this.button45.Text = "Сортировка по классам";
            this.button45.UseVisualStyleBackColor = true;
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox13.Location = new System.Drawing.Point(146, 416);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(282, 22);
            this.textBox13.TabIndex = 24;
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox15.Location = new System.Drawing.Point(146, 444);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(282, 22);
            this.textBox15.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(6, 419);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(142, 16);
            this.label19.TabIndex = 22;
            this.label19.Text = "Фамилия Родителя : ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(6, 447);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 16);
            this.label22.TabIndex = 21;
            this.label22.Text = "Имя Родителя : ";
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button27.Location = new System.Drawing.Point(9, 564);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(197, 42);
            this.button27.TabIndex = 20;
            this.button27.Text = "Обновить данные";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button26.Location = new System.Drawing.Point(9, 517);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(197, 41);
            this.button26.TabIndex = 19;
            this.button26.Text = "Удалить запись";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox24.Location = new System.Drawing.Point(146, 314);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(282, 22);
            this.textBox24.TabIndex = 18;
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox23.Location = new System.Drawing.Point(146, 286);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(282, 22);
            this.textBox23.TabIndex = 17;
            // 
            // listBox6
            // 
            this.listBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox6.FormattingEnabled = true;
            this.listBox6.ItemHeight = 16;
            this.listBox6.Items.AddRange(new object[] {
            "11А",
            "11Б",
            "10А",
            "10Б",
            "9А",
            "9Б",
            "8А",
            "8Б",
            "7А",
            "7Б",
            "6А",
            "6Б",
            "5А",
            "5Б",
            "4А",
            "4Б",
            "3А",
            "3Б",
            "2А",
            "2Б",
            "2В",
            "1А",
            "1Б",
            "1В"});
            this.listBox6.Location = new System.Drawing.Point(68, 112);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(72, 164);
            this.listBox6.TabIndex = 16;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(6, 112);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(52, 16);
            this.label39.TabIndex = 15;
            this.label39.Text = "Класс :";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox22.Location = new System.Drawing.Point(146, 78);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(282, 22);
            this.textBox22.TabIndex = 14;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(6, 342);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(126, 16);
            this.label38.TabIndex = 13;
            this.label38.Text = "Группа здоровья :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(6, 317);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(123, 16);
            this.label37.TabIndex = 12;
            this.label37.Text = "Место прописки : ";
            // 
            // monthCalendar7
            // 
            this.monthCalendar7.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.monthCalendar7.Location = new System.Drawing.Point(264, 112);
            this.monthCalendar7.Name = "monthCalendar7";
            this.monthCalendar7.TabIndex = 11;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(6, 289);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 16);
            this.label32.TabIndex = 10;
            this.label32.Text = "Адрес проживания : ";
            // 
            // listBox5
            // 
            this.listBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox5.FormattingEnabled = true;
            this.listBox5.ItemHeight = 16;
            this.listBox5.Items.AddRange(new object[] {
            "1 группа",
            "2 группа",
            "3 группа",
            "4 группа"});
            this.listBox5.Location = new System.Drawing.Point(146, 342);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(282, 68);
            this.listBox5.TabIndex = 9;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button9.Location = new System.Drawing.Point(9, 472);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(197, 39);
            this.button9.TabIndex = 8;
            this.button9.Text = "Добавить запись";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox20.Location = new System.Drawing.Point(146, 22);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(282, 22);
            this.textBox20.TabIndex = 7;
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox21.Location = new System.Drawing.Point(146, 50);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(282, 22);
            this.textBox21.TabIndex = 5;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(143, 112);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(116, 16);
            this.label33.TabIndex = 4;
            this.label33.Text = "Дата рождения : ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(6, 81);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(77, 16);
            this.label34.TabIndex = 3;
            this.label34.Text = "Отчество :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(6, 25);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(134, 16);
            this.label35.TabIndex = 1;
            this.label35.Text = "Фамилия ученика : ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(6, 53);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(101, 16);
            this.label36.TabIndex = 0;
            this.label36.Text = "Имя ученика : ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGridView_students);
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1003, 655);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Данные";
            // 
            // dataGridView_students
            // 
            this.dataGridView_students.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_students.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_students.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_students.Name = "dataGridView_students";
            this.dataGridView_students.Size = new System.Drawing.Size(991, 630);
            this.dataGridView_students.TabIndex = 0;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox18);
            this.tabPage8.Controls.Add(this.groupBox9);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(1455, 661);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Данные родителей";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.button47);
            this.groupBox18.Controls.Add(this.button29);
            this.groupBox18.Controls.Add(this.button28);
            this.groupBox18.Controls.Add(this.textBox31);
            this.groupBox18.Controls.Add(this.label50);
            this.groupBox18.Controls.Add(this.textBox30);
            this.groupBox18.Controls.Add(this.label49);
            this.groupBox18.Controls.Add(this.textBox25);
            this.groupBox18.Controls.Add(this.textBox26);
            this.groupBox18.Controls.Add(this.textBox27);
            this.groupBox18.Controls.Add(this.label43);
            this.groupBox18.Controls.Add(this.label44);
            this.groupBox18.Controls.Add(this.button10);
            this.groupBox18.Controls.Add(this.label46);
            this.groupBox18.Location = new System.Drawing.Point(1021, 12);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(429, 391);
            this.groupBox18.TabIndex = 7;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Работа с данными";
            // 
            // button47
            // 
            this.button47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button47.Location = new System.Drawing.Point(18, 342);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(405, 39);
            this.button47.TabIndex = 25;
            this.button47.Text = "Экспорт данных";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button29.Location = new System.Drawing.Point(18, 297);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(405, 39);
            this.button29.TabIndex = 24;
            this.button29.Text = "Обновить данные";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button28.Location = new System.Drawing.Point(18, 252);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(405, 39);
            this.button28.TabIndex = 23;
            this.button28.Text = "Удалить запись";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // textBox31
            // 
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox31.Location = new System.Drawing.Point(259, 92);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(164, 31);
            this.textBox31.TabIndex = 22;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.Location = new System.Drawing.Point(13, 95);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(123, 25);
            this.label50.TabIndex = 21;
            this.label50.Text = "Отчество : ";
            // 
            // textBox30
            // 
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox30.Location = new System.Drawing.Point(259, 55);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(164, 31);
            this.textBox30.TabIndex = 20;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.Location = new System.Drawing.Point(13, 58);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(122, 25);
            this.label49.TabIndex = 19;
            this.label49.Text = "Фамилия : ";
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox25.Location = new System.Drawing.Point(259, 166);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(164, 31);
            this.textBox25.TabIndex = 18;
            // 
            // textBox26
            // 
            this.textBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox26.Location = new System.Drawing.Point(259, 129);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(164, 31);
            this.textBox26.TabIndex = 17;
            // 
            // textBox27
            // 
            this.textBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.textBox27.Location = new System.Drawing.Point(259, 19);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(164, 31);
            this.textBox27.TabIndex = 14;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.Location = new System.Drawing.Point(13, 169);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(172, 25);
            this.label43.TabIndex = 12;
            this.label43.Text = "Место работы : ";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.Location = new System.Drawing.Point(13, 132);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(183, 25);
            this.label44.TabIndex = 10;
            this.label44.Text = "Номер телефона";
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(18, 207);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(405, 39);
            this.button10.TabIndex = 8;
            this.button10.Text = "Добавить запись";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(13, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(164, 25);
            this.label46.TabIndex = 3;
            this.label46.Text = "Имя родителя :";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dataGridView_parrents);
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(1012, 655);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Данные";
            // 
            // dataGridView_parrents
            // 
            this.dataGridView_parrents.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_parrents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_parrents.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_parrents.Name = "dataGridView_parrents";
            this.dataGridView_parrents.Size = new System.Drawing.Size(1000, 630);
            this.dataGridView_parrents.TabIndex = 0;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.button55);
            this.tabPage9.Controls.Add(this.button1);
            this.tabPage9.Controls.Add(this.groupBox10);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(1455, 661);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Данные учителей";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(343, 568);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(405, 45);
            this.button1.TabIndex = 3;
            this.button1.Text = "Обновить данные";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.dataGridView_teach);
            this.groupBox10.Location = new System.Drawing.Point(4, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1444, 558);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Данные";
            // 
            // dataGridView_teach
            // 
            this.dataGridView_teach.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView_teach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_teach.Location = new System.Drawing.Point(6, 19);
            this.dataGridView_teach.Name = "dataGridView_teach";
            this.dataGridView_teach.Size = new System.Drawing.Size(1432, 533);
            this.dataGridView_teach.TabIndex = 0;
            // 
            // button48
            // 
            this.button48.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button48.Location = new System.Drawing.Point(1047, 75);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(405, 57);
            this.button48.TabIndex = 5;
            this.button48.Text = "Экспорт данных";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button49
            // 
            this.button49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button49.Location = new System.Drawing.Point(6, 429);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(166, 40);
            this.button49.TabIndex = 17;
            this.button49.Text = "Экспорт данных";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button50
            // 
            this.button50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button50.Location = new System.Drawing.Point(6, 399);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(229, 39);
            this.button50.TabIndex = 18;
            this.button50.Text = "Экспорт данных";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button51
            // 
            this.button51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button51.Location = new System.Drawing.Point(231, 566);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(197, 39);
            this.button51.TabIndex = 27;
            this.button51.Text = "Экспорт данных";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // button52
            // 
            this.button52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button52.Location = new System.Drawing.Point(15, 589);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(339, 38);
            this.button52.TabIndex = 13;
            this.button52.Text = "Экспорт данных";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button53
            // 
            this.button53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button53.Location = new System.Drawing.Point(6, 577);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(341, 41);
            this.button53.TabIndex = 20;
            this.button53.Text = "Экспорт данных";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button54
            // 
            this.button54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button54.Location = new System.Drawing.Point(6, 592);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(352, 38);
            this.button54.TabIndex = 16;
            this.button54.Text = "Эспорт данных";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // button55
            // 
            this.button55.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button55.Location = new System.Drawing.Point(754, 568);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(405, 45);
            this.button55.TabIndex = 4;
            this.button55.Text = "Экспорт данных";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1471, 759);
            this.Controls.Add(this.tabControl_timetable);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_timetable)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_journal)).EndInit();
            this.tabControl_timetable.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ved)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_plan)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_final)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_meeting)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_lgot)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_uchet)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_students)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_parrents)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_teach)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem менюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem калькуляторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem игрыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem паукToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пасьянсToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem косынкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem подключениеКБазеДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверкаПодключенияКБазеДанныхToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView_journal;
        private System.Windows.Forms.TabControl tabControl_timetable;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView_timetable;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView_final;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGridView_plan;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dataGridView_meeting;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGridView_lgot;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dataGridView_uchet;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGridView_students;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dataGridView_parrents;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView dataGridView_teach;
        private System.Windows.Forms.ToolStripMenuItem панельАдминистратораToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.MonthCalendar monthCalendar3;
        private System.Windows.Forms.MonthCalendar monthCalendar2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.MonthCalendar monthCalendar4;
        private System.Windows.Forms.MonthCalendar monthCalendar5;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.MonthCalendar monthCalendar7;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView dataGridView_ved;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.MonthCalendar monthCalendar6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
    }
}


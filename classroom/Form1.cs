﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace classroom
{

    public partial class Form1 : Form
    {

        public Form1()
        {
			
			InitializeComponent();
			getData();
			getData2();
			getData3();
			getData4();
			getData5();
			getData6();
			getData7();
			getData8();
			getData9();
			getData10();
			getData11();
		}

		void exportData(DataGridView dataGrid)
		{
			Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
			Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook;
			Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet;
			ExcelWorkBook = ExcelApp.Workbooks.Add(System.Reflection.Missing.Value);
			ExcelWorkSheet = ExcelWorkBook.Worksheets.get_Item(1) as Microsoft.Office.Interop.Excel.Worksheet;

			for (int i = 0; i < dataGrid.Rows.Count; i++)
			{
				for (int j = 0; j < dataGrid.ColumnCount; j++)
				{
					ExcelApp.Cells[i + 1, j + 1] = dataGrid.Rows[i].Cells[j].Value;
				}
			}
			ExcelApp.Visible = true;
			ExcelApp.UserControl = true;
		}

		public void getData()
        {
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("SELECT * from `students`,`lessons`,`jounal` where `students`.id = `jounal`.StudenID AND jounal.LessonID = lessons.id;", db.GetConnection());
			
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_journal.Columns.Add("firstname", "Имя");
				dataGridView_journal.Columns.Add("secondname", "Фамилия");
				dataGridView_journal.Columns.Add("thirdname", "Отчество");
				dataGridView_journal.Columns.Add("name", "Предмет");
				dataGridView_journal.Columns.Add("number", "Оценка");
				while (mySqlDataReader.Read())
				{
					dataGridView_journal.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["name"].ToString(), mySqlDataReader["number"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData2()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("SELECT * from `timetable`,`lessons`,`teachers` where `timetable`.TeacherID = `teachers`.id AND timetable.LessonID = lessons.id;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_timetable.Columns.Add("Day", "День недели");
				dataGridView_timetable.Columns.Add("Name", "Название урока");
				dataGridView_timetable.Columns.Add("FirstName", "Имя учителя");
				dataGridView_timetable.Columns.Add("SecondName", "Фамилия Учителя");
				dataGridView_timetable.Columns.Add("ThirdName", "Отчество учителя");

				dataGridView_timetable.Columns.Add("Office", "Кабинет");
				while (mySqlDataReader.Read())
				{
					dataGridView_timetable.Rows.Add(mySqlDataReader["Day"].ToString(), mySqlDataReader["Name"].ToString(), mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["Office"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData5()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from finalevaluation,lessons,students,teachers where finalevaluation.StudenID = students.id AND finalevaluation.LessonID = lessons.id AND lessons.TeacherID = teachers.id;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

				dataGridView_final.Columns.Add("FirstName", "Имя ученика");
				dataGridView_final.Columns.Add("SecondName", "Фамилия ученика");
				dataGridView_final.Columns.Add("ThirdName", "Отчество ученика");
				dataGridView_final.Columns.Add("Evaluation", "Оценка");
				dataGridView_final.Columns.Add("Name", "Название предмета");
				
				while (mySqlDataReader.Read())
				{
					dataGridView_final.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["Evaluation"].ToString(), mySqlDataReader["Name"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData6()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from meeting;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

				dataGridView_meeting.Columns.Add("MeetingDate", "Дата собрания");
				dataGridView_meeting.Columns.Add("Other", "Повестка дня");

				while (mySqlDataReader.Read())
				{
					dataGridView_meeting.Rows.Add(mySqlDataReader["MeetingDate"].ToString(), mySqlDataReader["Other"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData7()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from benefits, benefitsid,students where benefits.StudentID=students.id AND benefits.BenefitsID = benefitsid.id; ", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

				dataGridView_lgot.Columns.Add("FirstName", "Имя ученика");
				dataGridView_lgot.Columns.Add("SecondName", "Фамилия ученика");
				dataGridView_lgot.Columns.Add("ThirdName", "Отчество ученика");
				dataGridView_lgot.Columns.Add("Name", "Наименование льготы");
				dataGridView_lgot.Columns.Add("StartDate", "Дата начала");
				dataGridView_lgot.Columns.Add("FinalDate", "Дата окончания");

				while (mySqlDataReader.Read())
				{
					dataGridView_lgot.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["Name"].ToString(), mySqlDataReader["StartDate"].ToString(), mySqlDataReader["FinalDate"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData8()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from parrents;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

				dataGridView_parrents.Columns.Add("PFirstName", "Имя родителя");
				dataGridView_parrents.Columns.Add("PSecondName", "Фамилия родителя");
				dataGridView_parrents.Columns.Add("PThirdName", "Отчество родителя");
				dataGridView_parrents.Columns.Add("PhoneNumber", "Имя телефона");
				dataGridView_parrents.Columns.Add("Work", "Место работы");

				while (mySqlDataReader.Read())
				{
					dataGridView_parrents.Rows.Add(mySqlDataReader["PFirstName"].ToString(), mySqlDataReader["PSecondName"].ToString(), mySqlDataReader["PThirdName"].ToString(), mySqlDataReader["PhoneNumber"].ToString(), mySqlDataReader["Work"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData3()
        {
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("SELECT * from `teachers`, `classes` where `teachers`.ClassID = `classes`.id;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_teach.Columns.Add("FirstName", "Имя учителя");
				dataGridView_teach.Columns.Add("SecondName", "Фамилия Учителя");
				dataGridView_teach.Columns.Add("ThirdName", "Отчество учителя");
				dataGridView_teach.Columns.Add("BirthdDate", "Дата рождения");
				dataGridView_teach.Columns.Add("Experience", "Стаж");
				dataGridView_teach.Columns.Add("WorkOffice", "Рабочий кабинет");
				dataGridView_teach.Columns.Add("PhoneNumber", "Номер телефона");
				dataGridView_teach.Columns.Add("Numbers", "Цифра класса");
				dataGridView_teach.Columns.Add("Characters", "Номер класса");
				while (mySqlDataReader.Read())
				{
					dataGridView_teach.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["BirthdDate"].ToString(), mySqlDataReader["Experience"].ToString(), mySqlDataReader["WorkOffice"].ToString(), mySqlDataReader["PhoneNumber"].ToString(), mySqlDataReader["Numbers"].ToString(), mySqlDataReader["Characters"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData4()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from plan;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_plan.Columns.Add("Date", "Дата");
				dataGridView_plan.Columns.Add("Place", "Место");
				dataGridView_plan.Columns.Add("Name", "Название мероприятия");

				dataGridView_plan.Columns.Add("Turnout", "Кол-во отсуствующих");
				dataGridView_plan.Columns.Add("Reason", "Кол-во отсуствующих");
				while (mySqlDataReader.Read())
				{
					dataGridView_plan.Rows.Add(mySqlDataReader["Date"].ToString(), mySqlDataReader["Place"].ToString(), mySqlDataReader["Name"].ToString(), mySqlDataReader["Turnout"].ToString(), mySqlDataReader["Reason"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData9()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from students,parrents,helthdata,classes where students.parrentid=parrents.id AND students.helthid = helthdata.id AND students.classid = classes.id;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_students.Columns.Add("FirstName", "Имя ученика");
				dataGridView_students.Columns.Add("SecondName", "Фамилия ученика");
				dataGridView_students.Columns.Add("ThirdName", "Отчество ученика мероприятия");
				dataGridView_students.Columns.Add("ResidenceAress", "Адрес проживания");
				dataGridView_students.Columns.Add("ActualAress", "Фактический адресс");
				dataGridView_students.Columns.Add("BirthdDate", "Дата рождения");
				dataGridView_students.Columns.Add("PFirstName", "Имя родителя");
				dataGridView_students.Columns.Add("PSecondName", "Фамилия родителя");
				dataGridView_students.Columns.Add("PThirdName", "Отчество родителя");
				dataGridView_students.Columns.Add("Name", "Группа здоровья");
				dataGridView_students.Columns.Add("Numbers", "Класс");
				dataGridView_students.Columns.Add("Characters", "Буква");

				while (mySqlDataReader.Read())
				{
					dataGridView_students.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["ResidenceAress"].ToString(), mySqlDataReader["ActualAress"].ToString(), mySqlDataReader["BirthdDate"].ToString(), 
						mySqlDataReader["PFirstName"].ToString(), mySqlDataReader["PSecondName"].ToString(), mySqlDataReader["PThirdName"].ToString(), mySqlDataReader["Name"].ToString(), mySqlDataReader["Numbers"].ToString(), mySqlDataReader["Characters"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData10()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from statement, students where statement.StudenID = students.id;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_ved.Columns.Add("FirstName", "Имя");
				dataGridView_ved.Columns.Add("SecondName", "Фамилия");
				dataGridView_ved.Columns.Add("ThirdName", "Отчество");
				dataGridView_ved.Columns.Add("Respectfully", "Кол-во по уважательной");
				dataGridView_ved.Columns.Add("Truancy", "Кол-во прогулов");
				dataGridView_ved.Columns.Add("desk1", "Причина по уважительной");
				dataGridView_ved.Columns.Add("desk2", "Причина прогула");

				while (mySqlDataReader.Read())
				{
					dataGridView_ved.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["Respectfully"].ToString(), mySqlDataReader["Truancy"].ToString(), mySqlDataReader["desk1"].ToString(), mySqlDataReader["desk2"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void getData11()
		{
			connector db = new connector();

			MySqlCommand mySqlCommand = new MySqlCommand("select * from accounting,accountingid,students,parrents,classes where accounting.accountingid = accountingid.id AND students.id = accounting.StudenID AND parrents.id = accounting.parrentid AND students.classid = classes.id;", db.GetConnection());

			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_uchet.Columns.Add("FirstName", "Имя ученика");
				dataGridView_uchet.Columns.Add("SecondName", "Фамилия ученика");
				dataGridView_uchet.Columns.Add("ThirdName", "Отчество ученика мероприятия");
				dataGridView_uchet.Columns.Add("ResidenceAress", "Адрес проживания");
				dataGridView_uchet.Columns.Add("ActualAress", "Фактический адресс");
				dataGridView_uchet.Columns.Add("BirthdDate", "Дата рождения");
				dataGridView_uchet.Columns.Add("PFirstName", "Имя родителя");
				dataGridView_uchet.Columns.Add("PSecondName", "Фамилия родителя");
				dataGridView_uchet.Columns.Add("PThirdName", "Отчество родителя");
				dataGridView_uchet.Columns.Add("PhoneNumber", "Номер телефона");
				dataGridView_uchet.Columns.Add("Work", "Работа");
				dataGridView_uchet.Columns.Add("Name", "Учет");
				dataGridView_uchet.Columns.Add("Numbers", "Класс");
				dataGridView_uchet.Columns.Add("Characters", "Буква");
				dataGridView_uchet.Columns.Add("StartDate", "Дата начала");
				dataGridView_uchet.Columns.Add("FinalDate", "Дата окончания");

				while (mySqlDataReader.Read())
				{
					dataGridView_uchet.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(),
						mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["ResidenceAress"].ToString(),
						mySqlDataReader["ActualAress"].ToString(), mySqlDataReader["BirthdDate"].ToString(),
						mySqlDataReader["PFirstName"].ToString(), mySqlDataReader["PSecondName"].ToString(),
						mySqlDataReader["PThirdName"].ToString(), mySqlDataReader["PhoneNumber"].ToString(),
						mySqlDataReader["Work"].ToString(), mySqlDataReader["Name"].ToString(),
						mySqlDataReader["Numbers"].ToString(), mySqlDataReader["Characters"].ToString(),
						mySqlDataReader["StartDate"].ToString(), mySqlDataReader["FinalDate"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox1.SelectedItem == "Математика") id = 1;
			if (listBox1.SelectedItem == "Русский язык") id = 2;
			if (listBox1.SelectedItem == "Английский язык") id = 3;
			if (listBox1.SelectedItem == "История России") id = 4;
			if (listBox1.SelectedItem == "Всеобщая история") id = 5;
			if (listBox1.SelectedItem == "Обществознание") id = 6;
			if (listBox1.SelectedItem == "Окружающий мир") id = 7;
			if (listBox1.SelectedItem == "Немецкий язык") id = 8;
			if (listBox1.SelectedItem == "Французкий язык") id = 9;
			if (listBox1.SelectedItem == "Физика") id = 10;
			if (listBox1.SelectedItem == "Алгебра") id = 11;
			if (listBox1.SelectedItem == "Геометрия") id = 12;
			if (listBox1.SelectedItem == "История родного края") id = 13;
			if (listBox1.SelectedItem == "Информатика") id = 14;
			if (listBox1.SelectedItem == "Черчение") id = 15;
			if (listBox1.SelectedItem == "Изобразительное исскуство") id = 16;
			if (listBox1.SelectedItem == "Здоровый образ жизни") id = 17;
			if (listBox1.SelectedItem == "Безопасность жизнедеятельности") id = 18;
			if (listBox1.SelectedItem == "Астраномия") id = 19;
			if (listBox1.SelectedItem == "Литература") id = 20;
			if (listBox1.SelectedItem == "Физкультура") id = 21;
			if (listBox1.SelectedItem == "Технология (мальчики)") id = 22;
			if (listBox1.SelectedItem == "Технология (девочки)") id = 23;
			if (listBox1.SelectedItem == "География") id = 24;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox1.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox3.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count == 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					string command_string = "INSERT INTO `jounal` VALUES (@Student, @id,@number)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Student", MySqlDbType.Int32).Value = Convert.ToInt32(student_id);
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = textBox2.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}

					
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}

		}

        private void button12_Click(object sender, EventArgs e)
        {
			dataGridView_journal.Columns.Remove("firstname");
			dataGridView_journal.Columns.Remove("secondname");
			dataGridView_journal.Columns.Remove("thirdname");
			dataGridView_journal.Columns.Remove("name");
			dataGridView_journal.Columns.Remove("number");
			getData();
		}

        private void button14_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox1.SelectedItem == "Математика") id = 1;
			if (listBox1.SelectedItem == "Русский язык") id = 2;
			if (listBox1.SelectedItem == "Английский язык") id = 3;
			if (listBox1.SelectedItem == "История России") id = 4;
			if (listBox1.SelectedItem == "Всеобщая история") id = 5;
			if (listBox1.SelectedItem == "Обществознание") id = 6;
			if (listBox1.SelectedItem == "Окружающий мир") id = 7;
			if (listBox1.SelectedItem == "Немецкий язык") id = 8;
			if (listBox1.SelectedItem == "Французкий язык") id = 9;
			if (listBox1.SelectedItem == "Физика") id = 10;
			if (listBox1.SelectedItem == "Алгебра") id = 11;
			if (listBox1.SelectedItem == "Геометрия") id = 12;
			if (listBox1.SelectedItem == "История родного края") id = 13;
			if (listBox1.SelectedItem == "Информатика") id = 14;
			if (listBox1.SelectedItem == "Черчение") id = 15;
			if (listBox1.SelectedItem == "Изобразительное исскуство") id = 16;
			if (listBox1.SelectedItem == "Здоровый образ жизни") id = 17;
			if (listBox1.SelectedItem == "Безопасность жизнедеятельности") id = 18;
			if (listBox1.SelectedItem == "Астраномия") id = 19;
			if (listBox1.SelectedItem == "Литература") id = 20;
			if (listBox1.SelectedItem == "Физкультура") id = 21;
			if (listBox1.SelectedItem == "Технология (мальчики)") id = 22;
			if (listBox1.SelectedItem == "Технология (девочки)") id = 23;
			if (listBox1.SelectedItem == "География") id = 24;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox1.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox3.Text;

			//string command = "DELETE FROM jounal WHERE id = @id AND name = @id AND number = @number;";

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count <= 1 && textBox2.Text.Length > 0 && textBox1.Text.Length > 0 && textBox3.Text.Length > 0)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					string command_string = "DELETE FROM `jounal` WHERE `StudenID` = @Student AND `LessonID` = @id AND `number` = @number;";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Student", MySqlDbType.Int32).Value = Convert.ToInt32(student_id);
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = textBox2.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				} else
                {
					MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button11_Click(object sender, EventArgs e)
        {
			

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox34.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox32.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count == 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					string command_string = "INSERT INTO `statement` VALUES (@Student, @Respectfully,@Truancy,@desk2,@desk1)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Student", MySqlDbType.Int32).Value = Convert.ToInt32(student_id);
					mySqlCommand.Parameters.Add("@Respectfully", MySqlDbType.Int32).Value = Convert.ToInt32(textBox33.Text);
					mySqlCommand.Parameters.Add("@Truancy", MySqlDbType.Int32).Value = Convert.ToInt32(textBox35.Text);
					mySqlCommand.Parameters.Add("@desk2", MySqlDbType.VarChar).Value = textBox37.Text;
					mySqlCommand.Parameters.Add("@desk1", MySqlDbType.VarChar).Value = textBox36.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button15_Click(object sender, EventArgs e)
        {
			dataGridView_ved.Columns.Remove("FirstName");
			dataGridView_ved.Columns.Remove("SecondName");
			dataGridView_ved.Columns.Remove("ThirdName");
			dataGridView_ved.Columns.Remove("Respectfully");
			dataGridView_ved.Columns.Remove("Truancy");
			dataGridView_ved.Columns.Remove("desk1");
			dataGridView_ved.Columns.Remove("desk2");
			getData10();
		}

        private void button13_Click(object sender, EventArgs e)
        {
			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox34.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox32.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count == 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					string command_string = "DELETE FROM `statement` WHERE StudenID = @Student AND Respectfully = @Respectfully AND Truancy=@Truancy AND desk2=@desk2 AND @desk1=desk1";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Student", MySqlDbType.Int32).Value = Convert.ToInt32(student_id);
					mySqlCommand.Parameters.Add("@Respectfully", MySqlDbType.Int32).Value = Convert.ToInt32(textBox33.Text);
					mySqlCommand.Parameters.Add("@Truancy", MySqlDbType.Int32).Value = Convert.ToInt32(textBox35.Text);
					mySqlCommand.Parameters.Add("@desk2", MySqlDbType.VarChar).Value = textBox37.Text;
					mySqlCommand.Parameters.Add("@desk1", MySqlDbType.VarChar).Value = textBox36.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button5_Click(object sender, EventArgs e)
        {
			connector db = new connector();

			try
			{
				db.OpenConnection();

				if (true)
				{
					DateTime date;
					date = monthCalendar6.SelectionStart;
					string command_string = "INSERT INTO `plan` (Date,Place,Name,Turnout,Reason) VALUES (@Date, @Place,@Name,@Turnout,@Reason)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Date", MySqlDbType.DateTime).Value = monthCalendar6.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@Place", MySqlDbType.VarChar).Value = textBox10.Text;
					mySqlCommand.Parameters.Add("@Name", MySqlDbType.VarChar).Value = textBox14.Text;
					mySqlCommand.Parameters.Add("@Turnout", MySqlDbType.VarChar).Value = textBox11.Text;
					mySqlCommand.Parameters.Add("@Reason", MySqlDbType.VarChar).Value = textBox12.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button17_Click(object sender, EventArgs e)
        {
			dataGridView_plan.Columns.Remove("Date");
			dataGridView_plan.Columns.Remove("Place");
			dataGridView_plan.Columns.Remove("Name");
			dataGridView_plan.Columns.Remove("Turnout");
			dataGridView_plan.Columns.Remove("Reason");
			getData4();
		}

        private void button16_Click(object sender, EventArgs e)
        {
			connector db = new connector();
			
			try
			{

				if (true)
				{
					db.OpenConnection();
					string command_string = "DELETE FROM `plan` WHERE Date=@Date AND Place=@Place AND Name=@Name AND Turnout=@Turnout AND Reason=@Reason";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Date", MySqlDbType.DateTime).Value = monthCalendar6.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@Place", MySqlDbType.VarChar).Value = textBox10.Text;
					mySqlCommand.Parameters.Add("@Name", MySqlDbType.VarChar).Value = textBox14.Text;
					mySqlCommand.Parameters.Add("@Turnout", MySqlDbType.VarChar).Value = textBox11.Text;
					mySqlCommand.Parameters.Add("@Reason", MySqlDbType.VarChar).Value = textBox12.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					//MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button1_Click(object sender, EventArgs e)
        {
			dataGridView_teach.Columns.Remove("FirstName");
			dataGridView_teach.Columns.Remove("SecondName");
			dataGridView_teach.Columns.Remove("ThirdName");
			dataGridView_teach.Columns.Remove("BirthdDate");
			dataGridView_teach.Columns.Remove("Experience");
			dataGridView_teach.Columns.Remove("WorkOffice");
			dataGridView_teach.Columns.Remove("PhoneNumber");
			dataGridView_teach.Columns.Remove("Numbers");
			dataGridView_teach.Columns.Remove("Characters");
			getData2();
		}

        private void button4_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox2.SelectedItem == "Математика") id = 1;
			if (listBox2.SelectedItem == "Русский язык") id = 2;
			if (listBox2.SelectedItem == "Английский язык") id = 3;
			if (listBox2.SelectedItem == "История России") id = 4;
			if (listBox2.SelectedItem == "Всеобщая история") id = 5;
			if (listBox2.SelectedItem == "Обществознание") id = 6;
			if (listBox2.SelectedItem == "Окружающий мир") id = 7;
			if (listBox2.SelectedItem == "Немецкий язык") id = 8;
			if (listBox2.SelectedItem == "Французкий язык") id = 9;
			if (listBox2.SelectedItem == "Физика") id = 10;
			if (listBox2.SelectedItem == "Алгебра") id = 11;
			if (listBox2.SelectedItem == "Геометрия") id = 12;
			if (listBox2.SelectedItem == "История родного края") id = 13;
			if (listBox2.SelectedItem == "Информатика") id = 14;
			if (listBox2.SelectedItem == "Черчение") id = 15;
			if (listBox2.SelectedItem == "Изобразительное исскуство") id = 16;
			if (listBox2.SelectedItem == "Здоровый образ жизни") id = 17;
			if (listBox2.SelectedItem == "Безопасность жизнедеятельности") id = 18;
			if (listBox2.SelectedItem == "Астраномия") id = 19;
			if (listBox2.SelectedItem == "Литература") id = 20;
			if (listBox2.SelectedItem == "Физкультура") id = 21;
			if (listBox2.SelectedItem == "Технология (мальчики)") id = 22;
			if (listBox2.SelectedItem == "Технология (девочки)") id = 23;
			if (listBox2.SelectedItem == "География") id = 24;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox6.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox4.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count > 0)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					string command_string = "INSERT INTO `finalevaluation` VALUES (@Student, @id,@number)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Student", MySqlDbType.Int32).Value = Convert.ToInt32(student_id);
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = Convert.ToInt32(textBox5.Text);
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}

		}

        private void button19_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox2.SelectedItem == "Математика") id = 1;
			if (listBox2.SelectedItem == "Русский язык") id = 2;
			if (listBox2.SelectedItem == "Английский язык") id = 3;
			if (listBox2.SelectedItem == "История России") id = 4;
			if (listBox2.SelectedItem == "Всеобщая история") id = 5;
			if (listBox2.SelectedItem == "Обществознание") id = 6;
			if (listBox2.SelectedItem == "Окружающий мир") id = 7;
			if (listBox2.SelectedItem == "Немецкий язык") id = 8;
			if (listBox2.SelectedItem == "Французкий язык") id = 9;
			if (listBox2.SelectedItem == "Физика") id = 10;
			if (listBox2.SelectedItem == "Алгебра") id = 11;
			if (listBox2.SelectedItem == "Геометрия") id = 12;
			if (listBox2.SelectedItem == "История родного края") id = 13;
			if (listBox2.SelectedItem == "Информатика") id = 14;
			if (listBox2.SelectedItem == "Черчение") id = 15;
			if (listBox2.SelectedItem == "Изобразительное исскуство") id = 16;
			if (listBox2.SelectedItem == "Здоровый образ жизни") id = 17;
			if (listBox2.SelectedItem == "Безопасность жизнедеятельности") id = 18;
			if (listBox2.SelectedItem == "Астраномия") id = 19;
			if (listBox2.SelectedItem == "Литература") id = 20;
			if (listBox2.SelectedItem == "Физкультура") id = 21;
			if (listBox2.SelectedItem == "Технология (мальчики)") id = 22;
			if (listBox2.SelectedItem == "Технология (девочки)") id = 23;
			if (listBox2.SelectedItem == "География") id = 24;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox6.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox4.Text;

			//string command = "DELETE FROM jounal WHERE id = @id AND name = @id AND number = @number;";

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count <= 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					string command_string = "DELETE FROM `finalevaluation` WHERE `StudenID` = @Student AND `LessonID` = @id AND `Evaluation` = @number;";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Student", MySqlDbType.Int32).Value = Convert.ToInt32(student_id);
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = textBox5.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button18_Click(object sender, EventArgs e)
        {
			dataGridView_final.Columns.Remove("FirstName");
			dataGridView_final.Columns.Remove("SecondName");
			dataGridView_final.Columns.Remove("ThirdName");
			dataGridView_final.Columns.Remove("Evaluation");
			dataGridView_final.Columns.Remove("Name");
			getData5();
		}

        private void button6_Click(object sender, EventArgs e)
        {
			connector db = new connector();

			try
			{
				db.OpenConnection();

				if (true)
				{
					string command_string = "INSERT INTO `meeting` (MeetingDate,Other) VALUES (@Date, @Place)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Date", MySqlDbType.DateTime).Value = monthCalendar1.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@Place", MySqlDbType.VarChar).Value = textBox8.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button20_Click(object sender, EventArgs e)
        {
			connector db = new connector();

			try
			{

				if (true)
				{
					db.OpenConnection();
					string command_string = "DELETE FROM `meeting` WHERE MeetingDate=@Date AND Other=@Place";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@Date", MySqlDbType.DateTime).Value = monthCalendar1.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@Place", MySqlDbType.VarChar).Value = textBox8.Text;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					//MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button21_Click(object sender, EventArgs e)
        {

			dataGridView_meeting.Columns.Remove("MeetingDate");
			dataGridView_meeting.Columns.Remove("Other");
			getData6();
		}

        private void button7_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox3.SelectedItem == "На бесплатное питание") id = 1;
			if (listBox3.SelectedItem == "На бесплатный проезд") id = 2;
			if (listBox3.SelectedItem == "Пенсия по потери кормильца") id = 3;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox17.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox7.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);
				//MessageBox.Show(student_id.ToString());
				if (dataTable.Rows.Count == 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					db.OpenConnection();
					string command_string = "INSERT INTO `benefits` VALUES (@id,@id2,@date1,@date2)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@date1", MySqlDbType.DateTime).Value = monthCalendar2.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@date2", MySqlDbType.DateTime).Value = monthCalendar3.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@id2", MySqlDbType.Int32).Value = student_id;
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button22_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox3.SelectedItem == "На бесплатное питание") id = 1;
			if (listBox3.SelectedItem == "На бесплатный проезд") id = 2;
			if (listBox3.SelectedItem == "Пенсия по потери кормильца") id = 3;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;

			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox17.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox7.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);
				//MessageBox.Show(student_id.ToString());
				if (dataTable.Rows.Count == 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					db.OpenConnection();
					string command_string = "DELETE FROM `benefits` WHERE BenefitsID=@id AND StudentID=@id2 AND StartDate=@date1 AND FinalDate=@date2";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@date1", MySqlDbType.DateTime).Value = monthCalendar2.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@date2", MySqlDbType.DateTime).Value = monthCalendar3.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@id2", MySqlDbType.Int32).Value = student_id;
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button23_Click(object sender, EventArgs e)
        {
			dataGridView_lgot.Columns.Remove("FirstName");
			dataGridView_lgot.Columns.Remove("SecondName");
			dataGridView_lgot.Columns.Remove("ThirdName");
			dataGridView_lgot.Columns.Remove("Name");
			dataGridView_lgot.Columns.Remove("StartDate");
			dataGridView_lgot.Columns.Remove("FinalDate");
			getData7();
		}

        private void button25_Click(object sender, EventArgs e)
        {
			dataGridView_uchet.Columns.Remove("FirstName");
			dataGridView_uchet.Columns.Remove("SecondName");
			dataGridView_uchet.Columns.Remove("ThirdName");
			dataGridView_uchet.Columns.Remove("ResidenceAress");
			dataGridView_uchet.Columns.Remove("ActualAress");
			dataGridView_uchet.Columns.Remove("BirthdDate");
			dataGridView_uchet.Columns.Remove("PFirstName");
			dataGridView_uchet.Columns.Remove("PSecondName");
			dataGridView_uchet.Columns.Remove("PThirdName");
			dataGridView_uchet.Columns.Remove("PhoneNumber");
			dataGridView_uchet.Columns.Remove("Work");
			dataGridView_uchet.Columns.Remove("Name");
			dataGridView_uchet.Columns.Remove("Numbers");
			dataGridView_uchet.Columns.Remove("Characters");
			dataGridView_uchet.Columns.Remove("StartDate");
			dataGridView_uchet.Columns.Remove("FinalDate");
			getData11();
		}

        private void button24_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox4.SelectedItem == "ПДН") id = 1;
			if (listBox4.SelectedItem == "Неблагополучная семья") id = 2;
			if (listBox4.SelectedItem == "Инвалид") id = 3;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object student_id = 0;
			object parrent_id = 0;

			string command_string_id = "SELECT id,parrentid from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox19.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox9.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);
				//MessageBox.Show(student_id.ToString());
				if (dataTable.Rows.Count == 1)
				{
					student_id = dataTable.Rows[0]["id"].ToString();
					parrent_id = dataTable.Rows[0]["parrentid"].ToString();
					db.OpenConnection();
					string command_string = "INSERT INTO `accounting` VALUES (@id,@id2,@id3,@date1,@date2)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = student_id;
					mySqlCommand.Parameters.Add("@id2", MySqlDbType.Int32).Value = parrent_id;
					mySqlCommand.Parameters.Add("@id3", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@date1", MySqlDbType.DateTime).Value = monthCalendar5.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@date2", MySqlDbType.DateTime).Value = monthCalendar4.SelectionStart.Date.ToString("yyyy-MM-dd");

					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}
				else
				{
					MessageBox.Show("Не все поля введены!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button10_Click(object sender, EventArgs e)
        {
			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			try
			{
				db.OpenConnection();

				if (true)
				{
					string command_string = "INSERT INTO `parrents` (PFirstName, PSecondName,PThirdName, PhoneNumber, Work) VALUES (@PFirstName, @PSecondName,@PThirdName, @PhoneNumber, @Work)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@PFirstName", MySqlDbType.VarChar).Value = textBox27.Text;
					mySqlCommand.Parameters.Add("@PSecondName", MySqlDbType.VarChar).Value = textBox30.Text;
					mySqlCommand.Parameters.Add("@PThirdName", MySqlDbType.VarChar).Value = textBox31.Text;
					mySqlCommand.Parameters.Add("@PhoneNumber", MySqlDbType.VarChar).Value = textBox26.Text;
					mySqlCommand.Parameters.Add("@Work", MySqlDbType.VarChar).Value = textBox25.Text;

					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button28_Click(object sender, EventArgs e)
        {
			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			try
			{
				db.OpenConnection();

				if (true)
				{
					string command_string = "DELETE FROM `parrents` WHERE PFirstName=@PFirstName AND PSecondName=@PSecondName AND PThirdName=@PThirdName AND PhoneNumber=@PhoneNumber AND Work=@Work";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@PFirstName", MySqlDbType.VarChar).Value = textBox27.Text;
					mySqlCommand.Parameters.Add("@PSecondName", MySqlDbType.VarChar).Value = textBox30.Text;
					mySqlCommand.Parameters.Add("@PThirdName", MySqlDbType.VarChar).Value = textBox31.Text;
					mySqlCommand.Parameters.Add("@PhoneNumber", MySqlDbType.VarChar).Value = textBox26.Text;
					mySqlCommand.Parameters.Add("@Work", MySqlDbType.VarChar).Value = textBox25.Text;

					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button29_Click(object sender, EventArgs e)
        {
			dataGridView_parrents.Columns.Remove("PFirstName");
			dataGridView_parrents.Columns.Remove("PSecondName");
			dataGridView_parrents.Columns.Remove("PThirdName");
			dataGridView_parrents.Columns.Remove("PhoneNumber");
			dataGridView_parrents.Columns.Remove("Work");
			getData8();
		}

        private void button9_Click(object sender, EventArgs e)
        {
			int id = 0;
			int id1 = 0;

			if (listBox5.SelectedItem == "1 группа") id = 1;
			if (listBox5.SelectedItem == "2 группа") id = 2;
			if (listBox5.SelectedItem == "3 группа") id = 3;
			if (listBox5.SelectedItem == "4 группа") id = 4;

			if (listBox6.SelectedItem == "11А") id1 = 1;
			if (listBox6.SelectedItem == "11Б") id1 = 2;
			if (listBox6.SelectedItem == "10А") id1 = 3;
			if (listBox6.SelectedItem == "10Б") id1 = 4;
			if (listBox6.SelectedItem == "9А") id1 = 5;
			if (listBox6.SelectedItem == "9Б") id1 = 6;
			if (listBox6.SelectedItem == "8А") id1 = 7;
			if (listBox6.SelectedItem == "8Б") id1 = 8;
			if (listBox6.SelectedItem == "7А") id1 = 9;
			if (listBox6.SelectedItem == "7Б") id1 = 10;
			if (listBox6.SelectedItem == "6А") id1 = 11;
			if (listBox6.SelectedItem == "6Б") id = 12;
			if (listBox6.SelectedItem == "5А") id = 13;
			if (listBox6.SelectedItem == "5Б") id = 14;
			if (listBox6.SelectedItem == "4А") id = 15;
			if (listBox6.SelectedItem == "4Б") id = 16;
			if (listBox6.SelectedItem == "3А") id = 17;
			if (listBox6.SelectedItem == "3Б") id = 18;
			if (listBox6.SelectedItem == "2А") id = 19;
			if (listBox6.SelectedItem == "2Б") id = 20;
			if (listBox6.SelectedItem == "2В") id = 21;
			if (listBox6.SelectedItem == "1А") id = 22;
			if (listBox6.SelectedItem == "1Б") id = 23;
			if (listBox6.SelectedItem == "1В") id = 24;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object parrent = 0;

			string command_string_id = "SELECT id from `parrents` WHERE `PFirstName`=@first OR `PSecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox15.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox13.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count == 1)
				{
					parrent = dataTable.Rows[0]["id"].ToString();
					string command_string = "INSERT INTO `students` (FirstName,SecondName,ThirdName,ParrentID,ResidenceAress,ActualAress,HelthID,BirthdDate,ClassID) VALUES (@FirstName,@SecondName,@ThirdName,@ParrentID,@ResidenceAress,@ActualAress,@HelthID,@BirthdDate,@ClassID)";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@FirstName", MySqlDbType.VarChar).Value = textBox21.Text;
					mySqlCommand.Parameters.Add("@SecondName", MySqlDbType.VarChar).Value = textBox20.Text;
					mySqlCommand.Parameters.Add("@ThirdName", MySqlDbType.VarChar).Value = textBox22.Text;
					mySqlCommand.Parameters.Add("@ParrentID", MySqlDbType.Int32).Value = parrent;
					mySqlCommand.Parameters.Add("@ResidenceAress", MySqlDbType.VarChar).Value = textBox23.Text;
					mySqlCommand.Parameters.Add("@ActualAress", MySqlDbType.VarChar).Value = textBox24.Text;
					mySqlCommand.Parameters.Add("@HelthID", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@BirthdDate", MySqlDbType.DateTime).Value = monthCalendar5.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@ClassID", MySqlDbType.Int32).Value = id1;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно добавлена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button26_Click(object sender, EventArgs e)
        {
			int id = 0;
			int id1 = 0;

			if (listBox5.SelectedItem == "1 группа") id = 1;
			if (listBox5.SelectedItem == "2 группа") id = 2;
			if (listBox5.SelectedItem == "3 группа") id = 3;
			if (listBox5.SelectedItem == "4 группа") id = 4;

			if (listBox6.SelectedItem == "11А") id1 = 1;
			if (listBox6.SelectedItem == "11Б") id1 = 2;
			if (listBox6.SelectedItem == "10А") id1 = 3;
			if (listBox6.SelectedItem == "10Б") id1 = 4;
			if (listBox6.SelectedItem == "9А") id1 = 5;
			if (listBox6.SelectedItem == "9Б") id1 = 6;
			if (listBox6.SelectedItem == "8А") id1 = 7;
			if (listBox6.SelectedItem == "8Б") id1 = 8;
			if (listBox6.SelectedItem == "7А") id1 = 9;
			if (listBox6.SelectedItem == "7Б") id1 = 10;
			if (listBox6.SelectedItem == "6А") id1 = 11;
			if (listBox6.SelectedItem == "6Б") id = 12;
			if (listBox6.SelectedItem == "5А") id = 13;
			if (listBox6.SelectedItem == "5Б") id = 14;
			if (listBox6.SelectedItem == "4А") id = 15;
			if (listBox6.SelectedItem == "4Б") id = 16;
			if (listBox6.SelectedItem == "3А") id = 17;
			if (listBox6.SelectedItem == "3Б") id = 18;
			if (listBox6.SelectedItem == "2А") id = 19;
			if (listBox6.SelectedItem == "2Б") id = 20;
			if (listBox6.SelectedItem == "2В") id = 21;
			if (listBox6.SelectedItem == "1А") id = 22;
			if (listBox6.SelectedItem == "1Б") id = 23;
			if (listBox6.SelectedItem == "1В") id = 24;

			connector db = new connector();
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();

			object parrent = 0;

			string command_string_id = "SELECT id from `parrents` WHERE `PFirstName`=@first OR `PSecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox15.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox13.Text;

			try
			{
				db.OpenConnection();

				dataAdapter.SelectCommand = sqlCommand1;
				dataAdapter.Fill(dataTable);

				if (dataTable.Rows.Count == 1)
				{
					parrent = dataTable.Rows[0]["id"].ToString();
					string command_string = "DELETE FROM `students` WHERE FirstName=@FirstName AND SecondName=@SecondName AND ThirdName=@ThirdName AND ParrentID=@ParrentID AND ResidenceAress=@ResidenceAress AND ActualAress=@ActualAress AND HelthID=@HelthID AND BirthdDate=@BirthdDate AND ClassID=@ClassID";
					MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
					mySqlCommand.Parameters.Add("@FirstName", MySqlDbType.VarChar).Value = textBox21.Text;
					mySqlCommand.Parameters.Add("@SecondName", MySqlDbType.VarChar).Value = textBox20.Text;
					mySqlCommand.Parameters.Add("@ThirdName", MySqlDbType.VarChar).Value = textBox22.Text;
					mySqlCommand.Parameters.Add("@ParrentID", MySqlDbType.Int32).Value = parrent;
					mySqlCommand.Parameters.Add("@ResidenceAress", MySqlDbType.VarChar).Value = textBox23.Text;
					mySqlCommand.Parameters.Add("@ActualAress", MySqlDbType.VarChar).Value = textBox24.Text;
					mySqlCommand.Parameters.Add("@HelthID", MySqlDbType.Int32).Value = id;
					mySqlCommand.Parameters.Add("@BirthdDate", MySqlDbType.DateTime).Value = monthCalendar5.SelectionStart.Date.ToString("yyyy-MM-dd");
					mySqlCommand.Parameters.Add("@ClassID", MySqlDbType.Int32).Value = id1;
					//MessageBox.Show(student_id.ToString());
					if (mySqlCommand.ExecuteNonQuery() == 1)
					{
						MessageBox.Show("Запись успешно удалена", "Успешно");
					}
					else
					{
						MessageBox.Show("Что-то пошло не так!", "Ошибка!");
					}
				}


			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button27_Click(object sender, EventArgs e)
        {
			dataGridView_students.Columns.Remove("FirstName");
			dataGridView_students.Columns.Remove("SecondName");
			dataGridView_students.Columns.Remove("ThirdName");
			dataGridView_students.Columns.Remove("ResidenceAress");
			dataGridView_students.Columns.Remove("ActualAress");
			dataGridView_students.Columns.Remove("BirthdDate");
			dataGridView_students.Columns.Remove("PFirstName");
			dataGridView_students.Columns.Remove("PSecondName");
			dataGridView_students.Columns.Remove("PThirdName");
			dataGridView_students.Columns.Remove("Name");
			dataGridView_students.Columns.Remove("Numbers");
			dataGridView_students.Columns.Remove("Characters");
			getData9();
		}

        private void button2_Click(object sender, EventArgs e)
        {
			dataGridView_timetable.Columns.Remove("Day");
			dataGridView_timetable.Columns.Remove("Name");
			dataGridView_timetable.Columns.Remove("FirstName");
			dataGridView_timetable.Columns.Remove("SecondName");
			dataGridView_timetable.Columns.Remove("ThirdName");
			dataGridView_timetable.Columns.Remove("Office");
			getData2();
		}


        private void button35_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_plan);

		}

        private void button48_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_timetable);
        }

        private void button52_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_journal);
        }

        private void button53_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_ved);
        }

        private void button54_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_final);
        }

        private void button39_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_meeting);
        }

        private void button49_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_lgot);
        }

        private void button50_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_uchet);
        }

        private void button51_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_students);
        }

        private void button47_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_parrents);
		}

        private void button55_Click(object sender, EventArgs e)
        {
			exportData(dataGridView_teach);
        }

        private void button30_Click(object sender, EventArgs e)
        {
			
			connector db = new connector();
			MySqlCommand mySqlCommand = new MySqlCommand("SELECT * from `students`,`lessons`,`jounal` where `students`.id = `jounal`.StudenID AND jounal.LessonID = lessons.id AND number=@number;", db.GetConnection());
			mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = textBox2.Text;
			

			try
			{
				db.OpenConnection();
				dataGridView_journal.Columns.Remove("firstname");
				dataGridView_journal.Columns.Remove("secondname");
				dataGridView_journal.Columns.Remove("thirdname");
				dataGridView_journal.Columns.Remove("name");
				dataGridView_journal.Columns.Remove("number");

				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_journal.Columns.Add("firstname", "Имя");
				dataGridView_journal.Columns.Add("secondname", "Фамилия");
				dataGridView_journal.Columns.Add("thirdname", "Отчество");
				dataGridView_journal.Columns.Add("name", "Предмет");
				dataGridView_journal.Columns.Add("number", "Оценка");
				while (mySqlDataReader.Read())
				{
					dataGridView_journal.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["name"].ToString(), mySqlDataReader["number"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button31_Click(object sender, EventArgs e)
        {
			connector db = new connector();

			object student_id = 0;
			DataTable dataTable = new DataTable();
			MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
			string command_string_id = "SELECT id from `students` WHERE `FirstName`=@first OR `SecondName`=@second";
			MySqlCommand sqlCommand1 = new MySqlCommand(command_string_id, db.GetConnection());
			sqlCommand1.Parameters.Add("@first", MySqlDbType.VarChar).Value = textBox1.Text;
			sqlCommand1.Parameters.Add("@second", MySqlDbType.VarChar).Value = textBox3.Text;



			if (dataTable.Rows.Count == 1)
			{
				student_id = dataTable.Rows[0]["id"].ToString();
				MySqlCommand mySqlCommand = new MySqlCommand("SELECT * from `students`,`lessons`,`jounal` where `students`.id = `jounal`.StudenID AND jounal.LessonID = lessons.id AND `students`.id=", db.GetConnection());
				mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = student_id;


				try
				{
					db.OpenConnection();
					dataGridView_journal.Columns.Remove("firstname");
					dataGridView_journal.Columns.Remove("secondname");
					dataGridView_journal.Columns.Remove("thirdname");
					dataGridView_journal.Columns.Remove("name");
					dataGridView_journal.Columns.Remove("number");

					MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
					dataGridView_journal.Columns.Add("firstname", "Имя");
					dataGridView_journal.Columns.Add("secondname", "Фамилия");
					dataGridView_journal.Columns.Add("thirdname", "Отчество");
					dataGridView_journal.Columns.Add("name", "Предмет");
					dataGridView_journal.Columns.Add("number", "Оценка");
					while (mySqlDataReader.Read())
					{
						dataGridView_journal.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["name"].ToString(), mySqlDataReader["number"].ToString());
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.ToString(), "Ошибка!");
				}
				finally
				{
					db.CloseConnection();
				}
			}
		}

        private void button32_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox1.SelectedItem == "Математика") id = 1;
			if (listBox1.SelectedItem == "Русский язык") id = 2;
			if (listBox1.SelectedItem == "Английский язык") id = 3;
			if (listBox1.SelectedItem == "История России") id = 4;
			if (listBox1.SelectedItem == "Всеобщая история") id = 5;
			if (listBox1.SelectedItem == "Обществознание") id = 6;
			if (listBox1.SelectedItem == "Окружающий мир") id = 7;
			if (listBox1.SelectedItem == "Немецкий язык") id = 8;
			if (listBox1.SelectedItem == "Французкий язык") id = 9;
			if (listBox1.SelectedItem == "Физика") id = 10;
			if (listBox1.SelectedItem == "Алгебра") id = 11;
			if (listBox1.SelectedItem == "Геометрия") id = 12;
			if (listBox1.SelectedItem == "История родного края") id = 13;
			if (listBox1.SelectedItem == "Информатика") id = 14;
			if (listBox1.SelectedItem == "Черчение") id = 15;
			if (listBox1.SelectedItem == "Изобразительное исскуство") id = 16;
			if (listBox1.SelectedItem == "Здоровый образ жизни") id = 17;
			if (listBox1.SelectedItem == "Безопасность жизнедеятельности") id = 18;
			if (listBox1.SelectedItem == "Астраномия") id = 19;
			if (listBox1.SelectedItem == "Литература") id = 20;
			if (listBox1.SelectedItem == "Физкультура") id = 21;
			if (listBox1.SelectedItem == "Технология (мальчики)") id = 22;
			if (listBox1.SelectedItem == "Технология (девочки)") id = 23;
			if (listBox1.SelectedItem == "География") id = 24;
			connector db = new connector();
			MySqlCommand mySqlCommand = new MySqlCommand("SELECT * from `students`,`lessons`,`jounal` where `students`.id = `jounal`.StudenID AND jounal.LessonID = lessons.id AND number=@number;", db.GetConnection());
			mySqlCommand.Parameters.Add("@number", MySqlDbType.Int32).Value = id;


			try
			{
				db.OpenConnection();
				dataGridView_journal.Columns.Remove("firstname");
				dataGridView_journal.Columns.Remove("secondname");
				dataGridView_journal.Columns.Remove("thirdname");
				dataGridView_journal.Columns.Remove("name");
				dataGridView_journal.Columns.Remove("number");

				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView_journal.Columns.Add("firstname", "Имя");
				dataGridView_journal.Columns.Add("secondname", "Фамилия");
				dataGridView_journal.Columns.Add("thirdname", "Отчество");
				dataGridView_journal.Columns.Add("name", "Предмет");
				dataGridView_journal.Columns.Add("number", "Оценка");
				while (mySqlDataReader.Read())
				{
					dataGridView_journal.Rows.Add(mySqlDataReader["FirstName"].ToString(), mySqlDataReader["SecondName"].ToString(), mySqlDataReader["ThirdName"].ToString(), mySqlDataReader["name"].ToString(), mySqlDataReader["number"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}
    }

}

